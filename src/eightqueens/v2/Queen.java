package eightqueens.v2;

import java.util.AbstractMap.SimpleEntry;

public class Queen extends SimpleEntry<Integer, Integer> {

	private static final long serialVersionUID = -7755203882053168598L;

	private Queen(Integer row, Integer column) {
		super(row, column);
	}

	public static Queen of(Integer left, Integer right) {
		return new Queen(left, right);
	}

	public Integer getRow() {
		return getKey();
	}

	public Integer getColumn() {
		return getValue();
	}
}
