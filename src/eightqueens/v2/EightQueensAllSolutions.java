package eightqueens.v2;

/**
 * 
 * @author nicholasilacqua
 *
 */
public class EightQueensAllSolutions extends AbstractEightQueens {

	protected boolean placeQueen(Integer column) {
		Integer nextColumn = column + 1;
		Integer currentRow = Integer.MIN_VALUE;

		while (currentRow != null) {
			currentRow = rowGaps.ceilingKey(currentRow + 1);
			if (isValidQueen(currentRow, column)) {
				setQueenData(currentRow, column);

				if (nextColumn >= size) {
					addToSolutions();
				}

				placeQueen(nextColumn);
				removeQueenData(currentRow, column);
			}
		}
		return false;
	}

}
