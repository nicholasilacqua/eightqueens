package eightqueens.v2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

/**
 * 
 * @author nicholasilacqua
 *
 */
public abstract class AbstractEightQueens {

	protected Integer size;

	// Data Structures
	protected Set<Integer> leftFacingDiagonalSet = new HashSet<>();
	protected Set<Integer> rightFacingDiagonalSet = new HashSet<>();
	protected TreeMap<Integer, Void> rowGaps = new TreeMap<>();

	protected List<Queen> queens = new ArrayList<>();
	protected List<List<Queen>> solutions = new ArrayList<>();

	protected abstract boolean placeQueen(Integer column);

	public List<List<Queen>> run(int size) {
		this.size = size;
		setGaps();
		placeQueen(0);
		return solutions;
	}

	private void setGaps() {
		for (int i = 0; i < size; i++) {
			rowGaps.put(i, null);
		}
	}

	protected boolean isValidQueen(Integer row, Integer column) {
		if (row == null) {
			return false;
		}

		Integer leftFacingDiagonal = getLeftFacingDiagnal(row, column);
		Integer rightFacingDiagonal = getRightFacingDiagnal(row, column);

		boolean leftFacingDiagonalIsAvailable = !leftFacingDiagonalSet.contains(leftFacingDiagonal);
		boolean rightFacingDiagonalIsAvailable = !rightFacingDiagonalSet.contains(rightFacingDiagonal);

		return leftFacingDiagonalIsAvailable && rightFacingDiagonalIsAvailable;
	}

	protected Integer getLeftFacingDiagnal(Integer row, Integer column) {
		return row + column;
	}

	protected Integer getRightFacingDiagnal(Integer row, Integer column) {
		return column + size - row;
	}

	protected void setQueenData(Integer row, Integer column) {
		leftFacingDiagonalSet.add(getLeftFacingDiagnal(row, column));
		rightFacingDiagonalSet.add(getRightFacingDiagnal(row, column));
		rowGaps.remove(row);
		queens.add(Queen.of(row, column));
	}

	protected void removeQueenData(Integer row, Integer column) {
		leftFacingDiagonalSet.remove(getLeftFacingDiagnal(row, column));
		rightFacingDiagonalSet.remove(getRightFacingDiagnal(row, column));
		rowGaps.put(row, null);
		// queens.remove(Pair.of(row, column));
		queens.remove(column.intValue());
	}

	protected void addToSolutions() {
		solutions.add(new ArrayList<Queen>(queens));
	}

	public void showDebug() {
		System.out.println("Queens : " + queens);
		System.out.println("Left Facing Diagonals : " + leftFacingDiagonalSet);
		System.out.println("Right Facing Diagonals : " + rightFacingDiagonalSet);
		System.out.println("Row Gaps : " + rowGaps);
	}

}
