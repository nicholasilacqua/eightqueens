package eightqueens.v2;

/**
 * 
 * @author nicholasilacqua
 *
 */
public class EightQueensSingleSolution extends AbstractEightQueens {

	protected boolean placeQueen(Integer column) {
		Integer nextColumn = column + 1;
		Integer currentRow = Integer.MIN_VALUE;

		while (currentRow != null) {
			currentRow = rowGaps.ceilingKey(currentRow + 1);
			if (isValidQueen(currentRow, column)) {
				setQueenData(currentRow, column);

				if (nextColumn >= size) {
					addToSolutions();
					return true;
				} else if (placeQueen(nextColumn)) {
					return true;
				} else {
					removeQueenData(currentRow, column);
				}
			}
		}
		return false;
	}
}
