package eightqueens.v2;

import java.util.List;

public class EightQueensDriver {

	// Settings
	private static boolean getAllSolutions = true;
	private static boolean displaySolutions = true;
	private static boolean debug = true;

	public static void main(String[] args) {
		int size = 8;

		AbstractEightQueens eightQueens = getAllSolutions ? new EightQueensAllSolutions()
				: new EightQueensSingleSolution();
		List<List<Queen>> solutions = eightQueens.run(size);
		if (debug) {
			eightQueens.showDebug();
		}

		displaySolutions(solutions, size);
	}

	// Display Methods and Reporting

	private static void displaySolutions(List<List<Queen>> solutions, int size) {

		System.out.println("Size of Board : " + size);
		System.out.println("Number of solutions : " + solutions.size());
		System.out.println();

		if (displaySolutions) {
			for (List<Queen> solution : solutions) {
				displayBoard(solution, size);
			}
		}
	}

	private static void displayBoard(List<Queen> solution, int size) {
		for (Queen queen : solution) {
			Integer column = queen.getColumn();
			for (int i = 0; i < size; i++) {
				System.out.print(String.format(" %s ", column == i ? "X" : "0"));
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}

}
