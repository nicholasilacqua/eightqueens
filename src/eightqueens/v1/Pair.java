package eightqueens.v1;

import java.util.AbstractMap.SimpleEntry;

public class Pair<K, V> extends SimpleEntry<K, V> {

	private static final long serialVersionUID = -7755203882053168598L;

	public Pair(K left, V right) {
		super(left, right);
	}

	public static <K, V> Pair<K, V> of(K left, V right) {
		return new Pair<K, V>(left, right);
	}

	public K getLeft() {
		return getKey();
	}

	public V getRight() {
		return getValue();
	}
}
