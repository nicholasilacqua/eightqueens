package eightqueens.v1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

/**
 * 
 * @author nicholasilacqua
 *
 */
public class EightQueensSingleSolution {

	// Settings

	private static boolean debug = false;

	// Start and End Values
	// Can handle negative start values

	private static Integer startValue = 1;
	private static Integer size = 8;
	private static Integer endValue = startValue + size;

	// Data Structures

	private static Set<Integer> leftFacingDiagonalSet = new HashSet<>();
	private static Set<Integer> rightFacingDiagonalSet = new HashSet<>();
	private static Set<Pair<Integer, Integer>> queens = new HashSet<>();
	private static TreeMap<Integer, Void> rowGaps = new TreeMap<>();

	public static void main(String[] args) {
		setGaps();
		placeQueen(startValue);
		displaySolution();
	}

	private static void setGaps() {
		for (int i = startValue; i < endValue; i++) {
			rowGaps.put(i, null);
		}
	}

	private static boolean placeQueen(Integer column) {
		Integer nextColumn = column + 1;
		Integer currentRow = Integer.MIN_VALUE;

		while (currentRow != null) {
			currentRow = rowGaps.ceilingKey(currentRow + 1);
			if (isValidQueen(currentRow, column)) {
				setQueenData(currentRow, column);

				if (nextColumn >= endValue || placeQueen(nextColumn)) {
					return true;
				}

				removeQueenData(currentRow, column);
			}
		}
		return false;
	}

	private static boolean isValidQueen(Integer row, Integer column) {
		if (row == null) {
			return false;
		}

		Integer leftFacingDiagonal = getLeftFacingDiagnal(row, column);
		Integer rightFacingDiagonal = getRightFacingDiagnal(row, column);

		boolean leftFacingDiagonalIsAvailable = !leftFacingDiagonalSet.contains(leftFacingDiagonal);
		boolean rightFacingDiagonalIsAvailable = !rightFacingDiagonalSet.contains(rightFacingDiagonal);

		return leftFacingDiagonalIsAvailable && rightFacingDiagonalIsAvailable;
	}

	private static Integer getLeftFacingDiagnal(Integer row, Integer column) {
		return row + column;
	}

	private static Integer getRightFacingDiagnal(Integer row, Integer column) {
		return column + endValue - row - 1;
	}

	private static void setQueenData(Integer row, Integer column) {
		leftFacingDiagonalSet.add(getLeftFacingDiagnal(row, column));
		rightFacingDiagonalSet.add(getRightFacingDiagnal(row, column));
		rowGaps.remove(row);
		queens.add(Pair.of(row, column));
	}

	private static void removeQueenData(Integer row, Integer column) {
		leftFacingDiagonalSet.remove(getLeftFacingDiagnal(row, column));
		rightFacingDiagonalSet.remove(getRightFacingDiagnal(row, column));
		rowGaps.put(row, null);
		queens.remove(Pair.of(row, column));
	}

	// Display Methods and Reporting

	private static void displaySolution() {
		if (debug) {
			System.out.println("Queens : " + queens);
			System.out.println("Left Facing Diagonals : " + leftFacingDiagonalSet);
			System.out.println("Right Facing Diagonals : " + rightFacingDiagonalSet);
			System.out.println("Row Gaps : " + rowGaps);
		}

		System.out.println("Size of Board : " + size);
		System.out.println();

		displayBoard();
	}

	private static void displayBoard() {
		List<Pair<Integer, Integer>> queeensList = new ArrayList<>(queens);
		queeensList.sort((first, second) -> Integer.compare(first.getLeft(), second.getLeft()));
		for (Pair<Integer, Integer> queen : queeensList) {
			Integer column = queen.getRight();
			for (int i = startValue; i < endValue; i++) {
				System.out.print(String.format(" %s ", column == i ? "X" : "0"));
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}

}
